using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoBotBlocker;
using System.Collections.Generic;

namespace ABBTests
{
    [TestClass]
    public class FirewallTests
    {
        [TestMethod]
        public void SetRule()
        {
            Windows win = new Windows(null);

            win.RunFirewallRules(new List<string> { "192.168.20.4", "192.168.40.5" });
        }
    }
}
