﻿using NetFwTypeLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace AutoBotBlocker
{
    public class Windows
    {
        public Action<object> Logger { get; set; }

        public Windows(Action<object> logger)
        {
            Logger = logger;
        }

        public void RunFirewallRules(List<string> ips)
        {
            const string NAME = "BOT_BLOCK";
            bool isNewRule = true;
            INetFwRule fwRule = null;
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            try
            {
                //Check if rule already exists
                fwRule = firewallPolicy.Rules.Item(NAME);
            }
            catch (System.IO.FileNotFoundException) { }

            isNewRule = fwRule == null;
            if (isNewRule)
            {
                //If rule does not exist yet: Create new
                fwRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            }

            //Rulefo
            fwRule.Name = NAME;
            fwRule.Description = "Blocking the Bot Net";
            fwRule.Enabled = true;

            fwRule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_ANY;
            fwRule.RemoteAddresses = string.Join(",", ips);
            fwRule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;
            fwRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
            fwRule.Profiles = (int)NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_ALL;

            //If the rule is new, add it!
            if (isNewRule)
            {
                firewallPolicy.Rules.Add(fwRule);
            }
        }

        public List<string> ReadEventLogs(DateTime checkAfter)
        {
            List<string> newIPs = new List<string>();
#if DEBUG
            //newIPs.Add("127.0.0.1");
            newIPs.Add("23.224.151.193");
#else
            EventLog evtlog = new EventLog("Security");

            int eventCount = evtlog.Entries.Count;
            EventLogEntry[] entries = new EventLogEntry[eventCount];
            evtlog.Entries.CopyTo(entries, 0);

            foreach (EventLogEntry entry in entries)
            {
                if (entry.InstanceId == 4625 && entry.TimeWritten >= checkAfter)
                {
                    Match matchResultRegex = Regex.Match(entry.Message, "(?<=Quellnetzwerkadresse\\:\t).*");
                    string matchResult = matchResultRegex.Value.Trim();
                    if (!string.IsNullOrWhiteSpace(matchResult) && !newIPs.Contains(matchResult) && !matchResult.StartsWith("192.168") && matchResult != "::1" && matchResult != "127.0.0.1")
                    {
                        if (Regex.IsMatch(matchResult, "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"))
                        {
                            newIPs.Add(matchResult);
                        }
                        else
                        {
                            Logger?.Invoke("WARNING! Weird IP! SKIPPING!");
                            Logger?.Invoke(entry.Message);
                        }
                    }
                }
            }
#endif

            return newIPs;
        }
    }
}
