﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutoBotBlocker
{
    public class AbuseIPDB
    {
        private static string ABUSEIPDB_KEY;
        private static readonly int CHECK_THRESHHOLD;
        private static readonly int REPORT_THRESHHOLD;

        private const int CHECK_DAYS = 30;
        public string Ip { get; private set; }
        public ABBCore Core { get; private set; }

        static AbuseIPDB()
        {
            ABUSEIPDB_KEY = ConfigurationManager.AppSettings["AbuseIPDBKey"];

            int.TryParse(ConfigurationManager.AppSettings["AbuseIPDBReportThreshold"], out REPORT_THRESHHOLD);
            int.TryParse(ConfigurationManager.AppSettings["AbuseIPDBCheckThreshold"], out CHECK_THRESHHOLD);

            if (string.IsNullOrWhiteSpace(ABUSEIPDB_KEY))
            {
                ABUSEIPDB_KEY = null;
            }
        }

        /// <summary>
        /// Counts the Ban Entries from the Abuse IP DB Query
        /// </summary>
        /// <returns>The Amount of reports in the last <see cref="CHECK_DAYS"/></returns>
        public int BanEntryCount { get; private set; } = 0;

        /// <summary>
        /// Counts the Ban Entries from the Abuse IP DB Query
        /// </summary>
        /// <returns>The Amount of reports in the last <see cref="CHECK_DAYS"/></returns>
        public int AbuseConfidance { get; private set; } = 0;

        /// <summary>
        /// Gets the CountryCode from a Abuse IP DB query
        /// </summary>
        public string CountryCode { get; private set; } = "??";

        /// <summary>
        /// Contains the AbuseDB's response
        /// </summary>
        public string Query { get; private set; } = string.Empty;

        /// <summary>
        /// Reports the associated IP for abusing the open RDP Port
        /// </summary>
        public void ReportIP()
        {
            bool mayContinue = true;

            #region [MAY CONTINUE]
            if (ABUSEIPDB_KEY == null || CHECK_THRESHHOLD == 0)
            {
                mayContinue = false;
            }

            if (mayContinue)
            {
                int prevReqCount = Core.Ipdb.QueryFirst<int>("SELECT COUNT(Time) FROM AbuseIPDBReports");

                if (prevReqCount > REPORT_THRESHHOLD)
                {
                    Core.Logger.Invoke($"WARNING Found {prevReqCount} reports in the last 24h!");
                    mayContinue = false;
                }
            }
            #endregion

            #region [REPORT!]
            try
            {
                Core.Ipdb.Execute("INSERT INTO AbuseIPDBReports VALUES(@ticks)", new { ticks = DateTime.Now.Ticks });

                using (HttpClient hc = new HttpClient())
                {
                    hc.DefaultRequestHeaders.Add("accept", "application/json");
                    hc.DefaultRequestHeaders.Add("Key", ABUSEIPDB_KEY);

                    string uri = $"https://api.abuseipdb.com/api/v2/report?ip={Ip}&categories=15,18&comment=Multiple%20brute%20forced%20RDP%20login%20attempts%20detected";

                    FormUrlEncodedContent content = new FormUrlEncodedContent(new KeyValuePair<string, string>[0]);
                    Task<HttpResponseMessage> queryTask = hc.PostAsync(new Uri(uri), content);

                    if (queryTask.Wait(60000))
                    {
                        Query = queryTask.Result.Content.ReadAsStringAsync().Result;
                    }
                    else if (queryTask.Exception != null)
                    {
                        throw new Exception($"AbuseIPDB report failure. Uri: \"{uri}\"", queryTask.Exception);
                    }
                }
            }
            catch (Exception ex)
            {
                Core.Logger.Invoke(ex);
            }
            #endregion
        }

        /// <summary>
        /// Initializes the object and queries AbuseIPDB
        /// </summary>
        /// <param name="ip">The IP to query</param>
        /// <param name="core">The core AutoBotBlocker meta object</param>
        public AbuseIPDB(ABBCore core, string ip)
        {
            Ip = ip;
            Core = core;
            bool mayContinue = true;

            #region [MAY CONTINUE]
            if (ABUSEIPDB_KEY == null || CHECK_THRESHHOLD == 0)
            {
                mayContinue = false;
            }

            if (mayContinue)
            {
                int prevReqCount = Core.Ipdb.QueryFirst<int>("SELECT COUNT(Time) FROM AbuseIPDBChecks");
                if (prevReqCount > CHECK_THRESHHOLD)
                {
                    Core.Logger.Invoke($"WARNING Found {prevReqCount} checks in the last 24h!");
                    mayContinue = false;
                }
            }
            #endregion
            
            if (mayContinue)
            {
                try
                {
                    Core.Ipdb.Execute("INSERT INTO AbuseIPDBChecks VALUES(@ticks)", new { ticks = DateTime.Now.Ticks });

                    #region [CHECK]
                    using (HttpClient hc = new HttpClient())
                    {
                        hc.DefaultRequestHeaders.Add("accept", "application/json");
                        hc.DefaultRequestHeaders.Add("Key", ABUSEIPDB_KEY);

                        //Do GET request
                        Uri uri = new Uri($"https://api.abuseipdb.com/api/v2/check?ipAddress={ip}&maxAgeInDays={CHECK_DAYS}");
                        Task<HttpResponseMessage> getTask = hc.GetAsync(uri);

                        if (!getTask.Wait(60000))
                        {
                            if (getTask.Exception != null)
                            {
                                throw new Exception("AbuseIPDB check error.", getTask.Exception);
                            }
                            else
                            {
                                throw new Exception($"AbuseIPDB check (timeout) error. URI: {uri}");
                            }
                        }

                        //Process response
                        HttpResponseMessage response = getTask.Result;
                        response.EnsureSuccessStatusCode();

                        //When the result was ok, get the content
                        Task<string> stringTask = response.Content.ReadAsStringAsync();
                        if (!stringTask.Wait(1000))
                        {
                            throw new Exception("Could not read check response", stringTask.Exception);
                        }
                        Query = stringTask.Result;
                    }
                    #endregion

                    #region [PARSE RESPONSE]
                    CountryCode = Regex.Match(Query, "(?<=\"countryCode\":\")\\w+?(?=\")").Value;
                    CountryCode = string.IsNullOrWhiteSpace(CountryCode) || CountryCode == "null" ? "??" : CountryCode;

                    int.TryParse(Regex.Match(Query, "(?<=\"abuseConfidenceScore\":).+?(?=,)").Value, out int abuseConfidance);
                    AbuseConfidance = abuseConfidance;

                    int.TryParse(Regex.Match(Query, "(?<=\"totalReports\":).+?(?=,)").Value, out int banCount);
                    BanEntryCount = banCount;
                    #endregion
                }
                catch (Exception ex)
                {
                    Core.Logger.Invoke(ex);
                }
            }
        }
    }
}