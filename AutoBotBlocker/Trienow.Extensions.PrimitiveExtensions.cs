﻿namespace Trienow.Extensions
{
    public static class PrimitiveExtensions
    {
        public static int ToInt(this bool v)
        {
            return v ? 1 : 0;
        }
    }
}
