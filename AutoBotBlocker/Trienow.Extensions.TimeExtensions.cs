﻿using System;

namespace Trienow.Extensions
{
    public static class TimeExtensions
    {
        public static readonly DateTime UNIX_ZERO = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ToUnixTimeStamp(this DateTime dateTime)
        {
            return (long)dateTime.ToUniversalTime().Subtract(UNIX_ZERO).TotalSeconds;
        }

        public static DateTime FromUnixTimeStamp(this long timeStamp)
        {
            return UNIX_ZERO.AddSeconds(timeStamp).ToLocalTime();
        }

        public static string ToDtString(this long ticks)
        {
            return new DateTime(ticks).ToString("yyyy-MM-dd HH:mm");
        }

        public static string ToLongDtString(this long ticks)
        {
            return new DateTime(ticks).ToString("u");
        }
    }
}
