﻿using AutoBotBlocker.Tables;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace AutoBotBlocker
{
    public class IPLogic
    {
        private const int MIN_REBAN_CONFIDNACE = 25;
        private readonly TimeSpan GRAY_PHASE = new TimeSpan(7, 0, 0, 0);

        private ABBCore core;

        private readonly string unbanListUrl;
        private string[] ipsToUnban = new string[0];
        private string[] ipsWhitelisted;

        private StringBuilder banLogBuilder = new StringBuilder();

        public bool RunFirewallRules { get; set; } = false;

        public IPLogic(ABBCore core)
        {
            this.core = core;

            ipsWhitelisted = ConfigurationManager.AppSettings["WhitelistedIPs"]?.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];

            unbanListUrl = ConfigurationManager.AppSettings["UnbanListURL"];
            if (string.IsNullOrWhiteSpace(unbanListUrl))
            {
                unbanListUrl = null;
            }
        }

        /// <summary>
        /// Adds an attempt to the ip
        /// </summary>
        /// <param name="ip">The IP to check</param>
        public void CanBan(string ip)
        {
            if (!ipsToUnban.Contains(ip))
            {
                Main entry = core.Ipdb.QueryFirstOrDefault<Main>($"SELECT * FROM Main WHERE IP = @ip", new { ip });

                if (entry == null)
                {
                    //Never seen this IP before
                    entry = new Main { IP = ip };
                    core.Ipdb.Execute($"INSERT INTO Main VALUES (NULL, @IP, @Country, @Unban, @Attempts, @LastAttempt, @BanConfidance, @TotalAttempts, @Banned, @BanCount)", entry);
                }
                else
                {
                    //Seen this IP before
                    entry.TotalAttempts++;

                    //Do a TS lookup!
                    string tsLookup = core.Tsdb.UsernamesFromIP(ip);
                    if (!string.IsNullOrWhiteSpace(tsLookup))
                    {
                        core.Logger?.Invoke($"TS: [{ip}] {tsLookup}");
                    }

                    //Whitelist lookup
                    bool isWhitelisted = false;
                    if (isWhitelisted = ipsWhitelisted.Contains(ip))
                    {
                        core.Logger?.Invoke($"WH: [{ip}]");
                    }

                    if (new DateTime(entry.LastAttempt) > DateTime.Now.Subtract(GRAY_PHASE) && !core.Tsdb.IsWhitelistedIP(ip) && !isWhitelisted)
                    {
                        //Seen it recently
                        entry.Attempts++;

                        if (entry.Attempts > 2 && entry.Banned == 0) //Sometimes the Bot spams logins so fast I get the IP twice
                        {
                            //Seen it multiple times recently BAN!
                            RunFirewallRules = true;

                            AbuseIPDB abuse = new AbuseIPDB(core, ip);
                            entry.BanConfidance = abuse.AbuseConfidance;
                            entry.Country = abuse.CountryCode;

                            DateTime unban = DateTime.Now;
                            if (abuse.AbuseConfidance > 5)
                            {
                                unban = unban.AddMonths(Math.Max(1, abuse.AbuseConfidance / 10));
                            }

                            double minutesSinceLastAttempt = DateTime.Now.Subtract(new DateTime(entry.LastAttempt)).TotalMinutes;
                            minutesSinceLastAttempt = Math.Max(0.5, minutesSinceLastAttempt);//My function gets too big between 0 and 0.5
                            minutesSinceLastAttempt = Math.Min(569, minutesSinceLastAttempt); //The point at which the ban time matches the last attempt time
                            double extraBanDays = 1 / minutesSinceLastAttempt * (abuse.CountryCode == "DE" ? 30 : 90);

                            unban = unban.AddDays(extraBanDays);

                            if (minutesSinceLastAttempt < 1 || entry.Attempts > 3)
                            {
                                abuse.ReportIP();
                            }

                            LogBan($"{ip,-15} -> extraBanDays: {extraBanDays,-5:F1} days + abuseConfidance: {abuse.AbuseConfidance:D3} = {unban:u}, timeSince: {minutesSinceLastAttempt,-5:F1}, Entry: {{{entry}}}");


                            entry.LastAttempt = DateTime.Now.Ticks;
                            entry.Unban = unban.Ticks;

                            core.Ipdb.Execute("UPDATE Main SET Attempts = @Attempts, Unban = @Unban, Country = @Country, LastAttempt = @LastAttempt, BanConfidance = @BanConfidance, TotalAttempts = @TotalAttempts WHERE ID = @ID", entry);
                        }
                        else
                        {
                            //Seen it only a few times
                            entry.LastAttempt = DateTime.Now.Ticks;
                            core.Ipdb.Execute("UPDATE Main SET Attempts = @Attempts, LastAttempt = @LastAttempt, TotalAttempts = @TotalAttempts WHERE ID = @ID", entry);
                        }
                    }
                    else
                    {
                        //Seen it long ago
                        entry.LastAttempt = DateTime.Now.Ticks;
                        entry.Attempts = 1;
                        core.Ipdb.Execute("UPDATE Main SET Attempts = @Attempts, LastAttempt = @LastAttempt, TotalAttempts = @TotalAttempts WHERE ID = @ID", entry);
                    }
                }
            }
        }

        /// <summary>
        /// Get the IPs to unban from a remote URL set in the App.config
        /// </summary>
        public void RetirieveUnbanIpList()
        {
#if !DEBUG
            if (unbanListUrl != null)
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string ipsToUnbanString = new WebClient().DownloadString(unbanListUrl).Trim();

                if (!string.IsNullOrWhiteSpace(ipsToUnbanString))
                {
                    RunFirewallRules = true;
                    ipsToUnban = ipsToUnbanString.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string ip in ipsToUnban)
                    {
                        Main entry = core.Ipdb.QueryFirstOrDefault<Main>($"SELECT * FROM Main WHERE IP = '{ip}'");
                        if (entry != null)
                        {
                            entry.Attempts = 0;
                            entry.Unban = DateTime.MinValue.Ticks;
                            entry.Banned = 0;

                            core.Ipdb.Execute("UPDATE Main SET Attempts = @Attempts, Unban = @Unban, Banned = @Banned WHERE ID = @ID", entry);
                        }
                    }
                }

                if (ipsToUnban.Length > 0)
                {
                    core.Logger?.Invoke("UNBANNING: " + string.Join(" | ", ipsToUnban));
                }
            }
#endif
        }

        /// <summary>
        /// Creates a List of IPs ready to be banned in the firewall
        /// </summary>
        public List<string> CompileFirewallRule()
        {
            List<string> firewallList = new List<string>();
            long currentTicks = DateTime.Now.Ticks;

            foreach (Main entry in core.Ipdb.Query<Main>($"SELECT * FROM Main"))
            {
                if (entry.Unban > currentTicks)
                {
                    //If the date for unbanning is in the future -> BAN!
                    firewallList.Add(entry.IP);

                    if (entry.Banned == 0)
                    {
                        entry.Banned = 1;
                        entry.BanCount++;

                        core.Ipdb.Execute("UPDATE Main SET Banned = @Banned, BanCount = @BanCount WHERE ID = @ID", entry);
                    }
                }
                else if (entry.Banned == 1)
                {
                    //If the IP is currently banned AND the date for unbanning is in the past
                    AbuseIPDB abuse = new AbuseIPDB(core, entry.IP);

                    entry.Country = abuse.CountryCode;
                    entry.BanConfidance = abuse.AbuseConfidance;

                    if (entry.BanConfidance > MIN_REBAN_CONFIDNACE)
                    {
                        entry.Unban = DateTime.Now.AddMonths(1).Ticks;
                        firewallList.Add(entry.IP);
                    }
                    else if (entry.Attempts > 0)
                    {
                        entry.Attempts = 0;
                        entry.Banned = 0;
                    }

                    core.Ipdb.Execute("UPDATE Main SET Attempts = @Attempts, Banned = @Banned, Unban = @Unban, Country = @Country, BanConfidance = @BanConfidance WHERE ID = @ID", entry);
                }
            }

            WriteBanLog();
            return firewallList;
        }

        #region [BAN LOGS]
        private void LogBan(object eventInfo)
        {
            string logLine = $"{DateTime.Now:u}\t{eventInfo}";
            Console.WriteLine(logLine);
            banLogBuilder.AppendLine(logLine);
        }

        private void WriteBanLog()
        {
            if (banLogBuilder != null && banLogBuilder.Length > 0)
            {
                File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ban.log"), banLogBuilder.ToString());
            }
        }
        #endregion

        public void Dispose()
        {
            lock (this)
            {
                try
                {
                    core.Ipdb.Close();
                    core.Ipdb.Dispose();
                }
                catch { }
            }
        }

        ~IPLogic()
        {
            Dispose();
        }
    }
}
