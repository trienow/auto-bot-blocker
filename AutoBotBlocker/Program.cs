﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;

namespace AutoBotBlocker
{
    class Program
    {
        static StringBuilder builder = new StringBuilder();
        static readonly string prevIpPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "prevIpList.sqlite");
        static readonly Mutex mutex = new Mutex(true, "{97665AEE-8A47-40AD-94EA-461655379A78}");

        static void Main(string[] args)
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                try
                {
                    Windows win = new Windows(LogEvent);

                    if (File.Exists(prevIpPath + ".kill"))
                    {
                        File.Move(prevIpPath, prevIpPath + ".bak");
                        win.RunFirewallRules(new List<string>());
                    }
                    else
                    {
                        using (ABBCore core = new ABBCore(prevIpPath, LogEvent))
                        {
                            IPLogic ipLogic = new IPLogic(core);
                            if (ipLogic.RunFirewallRules || (args.Length > 0 && args[0] == "--ban"))
                            {
                                try { ipLogic.RetirieveUnbanIpList(); } catch (Exception ex) { LogEvent(ex); }

                                foreach (string ip in win.ReadEventLogs(core.LastLogCheck()))
                                {
                                    ipLogic.CanBan(ip);
                                }

                                if (ipLogic.RunFirewallRules)
                                {
                                    win.RunFirewallRules(ipLogic.CompileFirewallRule());
                                }
                            }
                        }
                    }

                    mutex.ReleaseMutex();
                }
                catch (Exception ex) { LogEvent(ex); }
            }
            else { LogEvent("MUTEX LOCKED"); }

            SaveEvents();
        }

        private static void LogEvent(object eventInfo)
        {
            string logLine = $"{DateTime.Now:u}\t{eventInfo}";
            Console.WriteLine(logLine);
            builder.AppendLine(logLine);
        }

        private static void SaveEvents()
        {
            if (builder != null && builder.Length > 0)
            {
                File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "botBlock.log"), builder.ToString());
            }
        }
    }
}
