﻿using AutoBotBlocker.Tables;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using Trienow.Extensions;

namespace AutoBotBlocker
{
    public class TSDB
    {
        private string[] whitelist;
        private readonly bool useTSModule = false;
        private ABBCore core;

        public TSDB(ABBCore core)
        {
            this.core = core;

            whitelist = ConfigurationManager.AppSettings["WhitelistedUsers"]?.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];

            if (DateTime.Now > new DateTime(core.Setup.NextTSCacheTime))
            {
                CacheTSDB();

                int.TryParse(ConfigurationManager.AppSettings["TSDBCacheLeaseHours"], out int TSDBCacheLeaseHours);
                TSDBCacheLeaseHours = TSDBCacheLeaseHours < 6 ? 6 : TSDBCacheLeaseHours;

                core.Ipdb.Execute($"UPDATE Setup SET NextTSCacheTime = @nextCacheTime WHERE ID = 1", new { nextCacheTime = DateTime.Now.AddHours(TSDBCacheLeaseHours).Ticks });
            }

            CreateTSClientsTable();

            //When no clients have been cached into the ipdb, this Module will not be used
            useTSModule = core.Ipdb.QueryFirstOrDefault<int>("SELECT COUNT(ID) FROM TSClients") > 0;
        }

        private void CreateTSClientsTable()
        {
            core.Ipdb.Execute("CREATE TABLE IF NOT EXISTS `TSClients` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `UID` TEXT NOT NULL, `LastConnected` INTEGER NOT NULL, `Nickname` TEXT NOT NULL, `IP` TEXT NOT NULL)");
        }

        private bool CacheTSDB()
        {
            bool success = false;
            try
            {
                string path = ConfigurationManager.ConnectionStrings["TSDBPath"].ConnectionString;

                if (!string.IsNullOrWhiteSpace(path) && File.Exists(path))
                {
                    //TS locks the DB, so we need to copy it elsewhere
                    string tempFile = Path.GetTempFileName();
                    File.Copy(path, tempFile, true);

                    using (IDbConnection tsdb = new SQLiteConnection("DataSource=" + tempFile))
                    {

                        long unixTimeStamp = DateTime.Now.AddDays(-7).ToUnixTimeStamp();
                        IEnumerable<TSClient> clients = tsdb.Query<TSClient>("SELECT client_unique_id AS UID, client_lastconnected AS LastConnected, client_nickname AS Nickname, client_lastip AS IP FROM clients WHERE IP NOT NULL AND LastConnected > @minLastConnected", new {minLastConnected = DateTime.Now.AddDays(-7).ToUnixTimeStamp()});

                        core.Ipdb.Execute("DROP TABLE IF EXISTS TSClients");
                        CreateTSClientsTable();

                        core.Ipdb.Execute("INSERT INTO TSClients VALUES(NULL, @UID, @LastConnected, @Nickname, @IP)", clients);
                    }

                    //Delete the cached db
                    File.Delete(tempFile);
                }
                success = true;
            }
            catch (Exception ex)
            {
                core.Logger?.Invoke(ex);
            }

            return success;
        }

        public string UsernamesFromIP(string ip)
        {
            StringBuilder builder = new StringBuilder();

            if (useTSModule)
            {
                IEnumerable<TSClient> clients = core.Ipdb.Query<TSClient>("SELECT * FROM TSClients WHERE IP = @ip", new { ip });

                bool isFirst = true;
                foreach (TSClient client in clients)
                {
                    if (!isFirst)
                    {
                        builder.Append(" | ");
                    }

                    builder.Append("[").Append(client.UID).Append(", ").Append(client.Nickname).Append("]");

                    isFirst = false;
                }
            }

            return builder.ToString();
        }

        public bool IsWhitelistedIP(string ip)
        {
            if (useTSModule)
            {
                foreach (TSClient client in core.Ipdb.Query<TSClient>("SELECT UID FROM TSCLients WHERE IP = @ip", new { ip }))
                {
                    if (whitelist.Contains(client.UID))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
