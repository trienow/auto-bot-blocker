﻿using AutoBotBlocker.Tables;
using Dapper;
using System;
using System.Data;
using System.Data.SQLite;

namespace AutoBotBlocker
{
    public class ABBCore : IDisposable
    {
        public IDbConnection Ipdb { get; private set; }
        public Setup Setup { get; private set; }
        public TSDB Tsdb { get; private set; }

        public Action<object> Logger { get; private set; }

        public ABBCore(string ipdbPath, Action<object> logger)
        {
            Logger = logger;

            Ipdb = new SQLiteConnection("DataSource=" + ipdbPath);

            #region [TABLE CREATION]
            Ipdb.Execute("CREATE TABLE IF NOT EXISTS `Main` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `IP` TEXT NOT NULL UNIQUE, `Country` TEXT NOT NULL, `Unban` INTEGER NOT NULL, `Attempts` INTEGER NOT NULL, `LastAttempt` INTEGER NOT NULL, `BanConfidance` INTEGER NOT NULL, `TotalAttempts` INTEGER NOT NULL, `Banned` INTEGER NOT NULL, `BanCount` INTEGER NOT NULL)");
            Ipdb.Execute("CREATE INDEX IF NOT EXISTS `IPINDEX` ON `Main` (`IP` DESC);");
            Ipdb.Execute("CREATE TABLE IF NOT EXISTS `Setup` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `Version` INTEGER NOT NULL, `LastBanTime` INTEGER NOT NULL, `NextTSCacheTime` INTEGER NOT NULL)");
            Ipdb.Execute("CREATE TABLE IF NOT EXISTS `AbuseIPDBChecks` (`Time` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE)");
            Ipdb.Execute("CREATE TABLE IF NOT EXISTS `AbuseIPDBReports` (`Time` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE)");
            #endregion

            #region [GET SETUP VALUES]
            Setup = Ipdb.QueryFirstOrDefault<Setup>($"SELECT * FROM Setup");
            if (Setup == null)
            {
                Setup = new Setup(); //<- Init a setup table w/ default values
                Ipdb.Execute($"INSERT INTO Setup VALUES (NULL, @Version, @LastBanTime, @NextTSCacheTime)", Setup);
            }
            #endregion

            #region [VERSION UPGRADE]
            if (Setup.Version < Setup.DB_VERSION)
            {
                if (Setup.Version < 2)
                {
                    Ipdb.Execute("ALTER TABLE Main ADD COLUMN `BanConfidance` INTEGER NOT NULL DEFAULT 0");
                    Ipdb.Execute("ALTER TABLE Main ADD COLUMN `TotalAttempts` INTEGER NOT NULL DEFAULT 0");
                    Ipdb.Execute("ALTER TABLE Main ADD COLUMN `Banned` INTEGER NOT NULL DEFAULT 0");
                }

                if (Setup.Version < 3)
                {
                    Ipdb.Execute("ALTER TABLE Setup ADD COLUMN `NextTSCacheTime` INTEGER NOT NULL DEFAULT 0");
                }

                if (Setup.Version < 4)
                {
                    //Remove "AbuseReq" and "AbuseReqReset" columns from `Setup`
                    Ipdb.Execute("CREATE TABLE `temp_setup` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `Version` INTEGER NOT NULL, `LastBanTime` INTEGER NOT NULL, `NextTSCacheTime` INTEGER)");
                    Ipdb.Execute("INSERT INTO `temp_setup` SELECT `ID`,`Version`,`LastBanTime`,`NextTSCacheTime` FROM `Setup`");
                    Ipdb.Execute("DROP TABLE `Setup`");
                    Ipdb.Execute("ALTER TABLE `temp_setup` RENAME TO `Setup`");

                    Ipdb.Execute("ALTER TABLE Main ADD COLUMN `BanCount` INTEGER NOT NULL DEFAULT 0");
                }

                if (Setup.Version < 5)
                {
                    //We need a AbuseReport and a AbuseCheck table
                    Ipdb.Execute("DROP TABLE IF EXISTS `IpDbRequests`");
                }

                Setup.Version = Setup.DB_VERSION;
                Ipdb.Execute("UPDATE Setup SET Version = @Version WHERE ID = 1", Setup);
            }
            #endregion

            #region [RESET ABUSE REQUEST COUNTER]
            //I know it's only 24h, but just to be safe...
            Ipdb.Execute("DELETE FROM AbuseIPDBChecks WHERE Time < @ticks", new { ticks = DateTime.Now.AddHours(-25).Ticks });
            Ipdb.Execute("DELETE FROM AbuseIPDBReports WHERE Time < @ticks", new { ticks = DateTime.Now.AddHours(-25).Ticks });
            #endregion

            Tsdb = new TSDB(this);
        }

        public DateTime LastLogCheck()
        {
            Ipdb.Execute("UPDATE Setup SET LastBanTime = @ticks WHERE ID = 1", new { ticks = DateTime.Now.Ticks });
            return new DateTime(Setup.LastBanTime);
        }

        public void Dispose()
        {
            lock (this)
            {
                try
                {
                    Ipdb.Close();
                    Ipdb.Dispose();
                }
                catch { }
            }
        }

        ~ABBCore()
        {
            Dispose();
        }
    }
}
