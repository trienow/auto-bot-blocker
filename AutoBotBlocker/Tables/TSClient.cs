﻿namespace AutoBotBlocker.Tables
{
    public class TSClient
    {
        public string UID { get; set; }
        public uint LastConnected { get; set; }
        public string Nickname { get; set; }
        public string IP { get; set; }
    }
}