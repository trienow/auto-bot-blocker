﻿using System;

namespace AutoBotBlocker.Tables
{
    public class Setup
    {
        public const int DB_VERSION = 5;

        public int ID { get; set; } = 1;
        public int Version { get; set; } = DB_VERSION;
        public long LastBanTime { get; set; } = DateTime.MinValue.Ticks;
        public long NextTSCacheTime { get; set; } = DateTime.MinValue.Ticks;
    }
}
