﻿using System;
using System.Collections.Generic;
using Trienow.Extensions;

namespace AutoBotBlocker.Tables
{
    public class Main : IEquatable<Main>
    {
        public int ID { get; set; }
        public string IP { get; set; }
        public string Country { get; set; } = "??";
        public long Unban { get; set; } = DateTime.Now.Ticks;
        public int Attempts { get; set; } = 1;
        public long LastAttempt { get; set; } = DateTime.Now.Ticks;
        public int BanConfidance { get; set; } = 0;
        public int TotalAttempts { get; set; } = 1;
        public int Banned { get; set; } = 0;
        public int BanCount { get; set; } = 0;

        public override bool Equals(object obj) => Equals(obj as Main);
        public bool Equals(Main other)
        {
            return other != null
                && ID == other.ID
                && IP == other.IP
                && Country == other.Country
                && Unban == other.Unban 
                && Attempts == other.Attempts
                && LastAttempt == other.LastAttempt 
                && BanConfidance == other.BanConfidance
                && TotalAttempts == other.TotalAttempts 
                && Banned == other.Banned
                && BanCount == other.BanCount;
        }

        public override string ToString()
        {
            return $"ID: {ID}, " +
                $"IP: {IP}, " +
                $"Country: {Country}, " +
                $"Unban: {Unban.ToLongDtString()}, " +
                $"Attempts: {Attempts}, " +
                $"LastAttempt: {LastAttempt.ToLongDtString()}, " +
                $"BanConfidance: {BanConfidance}, " +
                $"TotalAttempts: {TotalAttempts}, " +
                $"Banned: {Banned}, " +
                $"BanCount: {BanCount}";
        }

        public override int GetHashCode()
        {
            var hashCode = -376002436;
            hashCode = hashCode * -1521134295 + ID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(IP);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Country);
            hashCode = hashCode * -1521134295 + Unban.GetHashCode();
            hashCode = hashCode * -1521134295 + Attempts.GetHashCode();
            hashCode = hashCode * -1521134295 + LastAttempt.GetHashCode();
            hashCode = hashCode * -1521134295 + BanConfidance.GetHashCode();
            hashCode = hashCode * -1521134295 + TotalAttempts.GetHashCode();
            hashCode = hashCode * -1521134295 + Banned.GetHashCode();
            hashCode = hashCode * -1521134295 + BanCount.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Main entry1, Main entry2)
        {
            return EqualityComparer<Main>.Default.Equals(entry1, entry2);
        }

        public static bool operator !=(Main entry1, Main entry2)
        {
            return !(entry1 == entry2);
        }
    }
}
