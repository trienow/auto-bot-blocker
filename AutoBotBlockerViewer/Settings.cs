﻿namespace AutoBotBlockerViewer
{
    class Settings
    {
        public const string FILE = "abbv.sqlite";
        public const string CREATE = "CREATE TABLE IF NOT EXISTS `Settings` (`ID` TEXT NOT NULL PRIMARY KEY UNIQUE, `Version` INTEGER NOT NULL, `AutoRefresh` INTEGER NOT NULL, `Highlight` INTEGER NOT NULL, `ScrollBottom` INTEGER NOT NULL, `Filters` TEXT NOT NULL, `FilterStates` TEXT NOT NULL)";
        public const string INSERT = "INSERT INTO Settings VALUES(@ID, @Version, @AutoRefresh, @Highlight, @ScrollBottom, @Filters, @FilterStates)";
        public const string UPDATE = "UPDATE Settings SET Version = @Version, AutoRefresh = @AutoRefresh, Highlight = @Highlight, ScrollBottom = @ScrollBottom, Filters = @Filters, FilterStates = @FilterStates WHERE ID = @ID";
        public const int CURRENT_VERSION = 2;

        public string ID { get; set; } = string.Empty;
        public int Version { get; set; } = CURRENT_VERSION;
        public int AutoRefresh { get; set; } = 1;
        public int Highlight { get; set; } = 1;
        public int ScrollBottom { get; set; } = 1;
        public string Filters { get; set; } = "SELECT *|FROM Main|WHERE LastAttempt > -24h|ORDER BY LastAttempt";
        public string FilterStates { get; set; } = "1111";
    }
}
