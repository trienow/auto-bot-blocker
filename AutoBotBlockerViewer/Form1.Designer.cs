﻿namespace AutoBotBlockerViewer
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "99999",
            "555.555.555.555",
            "999",
            "555",
            "9999-99-99 99:99",
            "9999-99-99 99:99",
            "@@",
            "100",
            "0",
            "0"}, -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listView = new System.Windows.Forms.ListView();
            this.cHID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAttempts = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTotalAttempts = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chUnban = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chLastAttempt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chCountry = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chBanConfidance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chBanned = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chBanCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ctxMenuListView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyDisplayedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyUnderlyingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chlistFilters = new System.Windows.Forms.ListView();
            this.chExistingFilters = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filterMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.filterDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.llStatement = new System.Windows.Forms.LinkLabel();
            this.btnAddFilter = new System.Windows.Forms.Button();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.reloadDBNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chbxAutoRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.chbxHighlight = new System.Windows.Forms.ToolStripMenuItem();
            this.chbxScrollBottom = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownSetupTable = new System.Windows.Forms.ToolStripDropDownButton();
            this.ctxMenuSetupTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastBanTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abuseIPDBReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abuseIPDBChecksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextTSCacheTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblShownValues = new System.Windows.Forms.ToolStripStatusLabel();
            this.fsWatcher = new System.IO.FileSystemWatcher();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ctxMenuListView.SuspendLayout();
            this.filterMenuStrip.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.ctxMenuSetupTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fsWatcher)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chlistFilters);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(1118, 636);
            this.splitContainer1.SplitterDistance = 860;
            this.splitContainer1.TabIndex = 0;
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cHID,
            this.chIP,
            this.chAttempts,
            this.chTotalAttempts,
            this.chUnban,
            this.chLastAttempt,
            this.chCountry,
            this.chBanConfidance,
            this.chBanned,
            this.chBanCount});
            this.listView.ContextMenuStrip = this.ctxMenuListView;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.FullRowSelect = true;
            this.listView.GridLines = true;
            this.listView.HideSelection = false;
            this.listView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowGroups = false;
            this.listView.Size = new System.Drawing.Size(860, 636);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // cHID
            // 
            this.cHID.Text = "ID";
            this.cHID.Width = 48;
            // 
            // chIP
            // 
            this.chIP.Text = "IP";
            this.chIP.Width = 105;
            // 
            // chAttempts
            // 
            this.chAttempts.Text = "Attempts";
            this.chAttempts.Width = 67;
            // 
            // chTotalAttempts
            // 
            this.chTotalAttempts.Text = "TotalAttempts";
            this.chTotalAttempts.Width = 96;
            // 
            // chUnban
            // 
            this.chUnban.Text = "Unban";
            this.chUnban.Width = 115;
            // 
            // chLastAttempt
            // 
            this.chLastAttempt.Text = "LastAttempt";
            this.chLastAttempt.Width = 110;
            // 
            // chCountry
            // 
            this.chCountry.Text = "Country";
            this.chCountry.Width = 57;
            // 
            // chBanConfidance
            // 
            this.chBanConfidance.Text = "BanConfidance";
            this.chBanConfidance.Width = 104;
            // 
            // chBanned
            // 
            this.chBanned.Text = "Banned";
            // 
            // chBanCount
            // 
            this.chBanCount.Text = "BanCount";
            this.chBanCount.Width = 71;
            // 
            // ctxMenuListView
            // 
            this.ctxMenuListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyDisplayedToolStripMenuItem,
            this.copyUnderlyingToolStripMenuItem});
            this.ctxMenuListView.Name = "ctxMenuListView";
            this.ctxMenuListView.Size = new System.Drawing.Size(164, 48);
            // 
            // copyDisplayedToolStripMenuItem
            // 
            this.copyDisplayedToolStripMenuItem.Image = global::AutoBotBlockerViewer.Properties.Resources.copy;
            this.copyDisplayedToolStripMenuItem.Name = "copyDisplayedToolStripMenuItem";
            this.copyDisplayedToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.copyDisplayedToolStripMenuItem.Text = "Copy Displayed";
            this.copyDisplayedToolStripMenuItem.Click += new System.EventHandler(this.CopyDisplayedToolStripMenuItem_Click);
            // 
            // copyUnderlyingToolStripMenuItem
            // 
            this.copyUnderlyingToolStripMenuItem.Image = global::AutoBotBlockerViewer.Properties.Resources.copyInv;
            this.copyUnderlyingToolStripMenuItem.Name = "copyUnderlyingToolStripMenuItem";
            this.copyUnderlyingToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.copyUnderlyingToolStripMenuItem.Text = "Copy Underlying";
            this.copyUnderlyingToolStripMenuItem.Click += new System.EventHandler(this.CopyUnderlyingToolStripMenuItem_Click);
            // 
            // chlistFilters
            // 
            this.chlistFilters.CheckBoxes = true;
            this.chlistFilters.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chExistingFilters});
            this.chlistFilters.ContextMenuStrip = this.filterMenuStrip;
            this.chlistFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chlistFilters.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chlistFilters.GridLines = true;
            this.chlistFilters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.chlistFilters.LabelEdit = true;
            this.chlistFilters.Location = new System.Drawing.Point(0, 101);
            this.chlistFilters.Name = "chlistFilters";
            this.chlistFilters.Size = new System.Drawing.Size(254, 513);
            this.chlistFilters.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.chlistFilters.TabIndex = 0;
            this.chlistFilters.UseCompatibleStateImageBehavior = false;
            this.chlistFilters.View = System.Windows.Forms.View.Details;
            this.chlistFilters.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.ChlistFilters_AfterLabelEdit);
            this.chlistFilters.BeforeLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.ChlistFilters_BeforeLabelEdit);
            this.chlistFilters.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.WriteSettings);
            // 
            // chExistingFilters
            // 
            this.chExistingFilters.Text = "ExistingFilters";
            this.chExistingFilters.Width = 247;
            // 
            // filterMenuStrip
            // 
            this.filterMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterDeleteToolStripMenuItem});
            this.filterMenuStrip.Name = "filterMenuStrip";
            this.filterMenuStrip.Size = new System.Drawing.Size(136, 26);
            // 
            // filterDeleteToolStripMenuItem
            // 
            this.filterDeleteToolStripMenuItem.Name = "filterDeleteToolStripMenuItem";
            this.filterDeleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.filterDeleteToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.filterDeleteToolStripMenuItem.Text = "Delete";
            this.filterDeleteToolStripMenuItem.Click += new System.EventHandler(this.FilterDeleteToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.llStatement);
            this.groupBox3.Controls.Add(this.btnAddFilter);
            this.groupBox3.Controls.Add(this.txtStatement);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(254, 77);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "New Filter";
            // 
            // llStatement
            // 
            this.llStatement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llStatement.AutoSize = true;
            this.llStatement.Location = new System.Drawing.Point(6, 23);
            this.llStatement.Name = "llStatement";
            this.llStatement.Size = new System.Drawing.Size(58, 13);
            this.llStatement.TabIndex = 5;
            this.llStatement.TabStop = true;
            this.llStatement.Text = "Statement:";
            this.llStatement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.llStatement.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LlStatement_LinkClicked);
            // 
            // btnAddFilter
            // 
            this.btnAddFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddFilter.Location = new System.Drawing.Point(3, 51);
            this.btnAddFilter.Name = "btnAddFilter";
            this.btnAddFilter.Size = new System.Drawing.Size(248, 23);
            this.btnAddFilter.TabIndex = 4;
            this.btnAddFilter.Text = "Add Filter";
            this.btnAddFilter.UseVisualStyleBackColor = true;
            this.btnAddFilter.Click += new System.EventHandler(this.BtnAddFilter_Click);
            // 
            // txtStatement
            // 
            this.txtStatement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatement.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatement.Location = new System.Drawing.Point(70, 20);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.Size = new System.Drawing.Size(175, 22);
            this.txtStatement.TabIndex = 3;
            this.txtStatement.Enter += new System.EventHandler(this.TxtStatement_Enter);
            this.txtStatement.Leave += new System.EventHandler(this.TxtStatement_Leave);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadDBNowToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(254, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // reloadDBNowToolStripMenuItem
            // 
            this.reloadDBNowToolStripMenuItem.Image = global::AutoBotBlockerViewer.Properties.Resources.recycle;
            this.reloadDBNowToolStripMenuItem.Name = "reloadDBNowToolStripMenuItem";
            this.reloadDBNowToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.reloadDBNowToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.reloadDBNowToolStripMenuItem.Text = "Reload DB Now";
            this.reloadDBNowToolStripMenuItem.Click += new System.EventHandler(this.BtnReload_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chbxAutoRefresh,
            this.chbxHighlight,
            this.chbxScrollBottom});
            this.optionsToolStripMenuItem.Image = global::AutoBotBlockerViewer.Properties.Resources.Plires;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // chbxAutoRefresh
            // 
            this.chbxAutoRefresh.Checked = true;
            this.chbxAutoRefresh.CheckOnClick = true;
            this.chbxAutoRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxAutoRefresh.Name = "chbxAutoRefresh";
            this.chbxAutoRefresh.Size = new System.Drawing.Size(187, 22);
            this.chbxAutoRefresh.Text = "Auto Refresh";
            this.chbxAutoRefresh.CheckedChanged += new System.EventHandler(this.ChbxAutoRefresh_CheckedChanged);
            this.chbxAutoRefresh.Click += new System.EventHandler(this.WriteSettings);
            // 
            // chbxHighlight
            // 
            this.chbxHighlight.Checked = true;
            this.chbxHighlight.CheckOnClick = true;
            this.chbxHighlight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxHighlight.Name = "chbxHighlight";
            this.chbxHighlight.Size = new System.Drawing.Size(187, 22);
            this.chbxHighlight.Text = "Highlight new Entries";
            this.chbxHighlight.Click += new System.EventHandler(this.WriteSettings);
            // 
            // chbxScrollBottom
            // 
            this.chbxScrollBottom.Checked = true;
            this.chbxScrollBottom.CheckOnClick = true;
            this.chbxScrollBottom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbxScrollBottom.Name = "chbxScrollBottom";
            this.chbxScrollBottom.Size = new System.Drawing.Size(187, 22);
            this.chbxScrollBottom.Text = "Scroll to Bottom";
            this.chbxScrollBottom.Click += new System.EventHandler(this.WriteSettings);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownSetupTable,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel1,
            this.lblShownValues});
            this.statusStrip1.Location = new System.Drawing.Point(0, 614);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(254, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDropDownSetupTable
            // 
            this.toolStripDropDownSetupTable.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDropDownSetupTable.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownSetupTable.DropDown = this.ctxMenuSetupTable;
            this.toolStripDropDownSetupTable.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownSetupTable.Name = "toolStripDropDownSetupTable";
            this.toolStripDropDownSetupTable.Size = new System.Drawing.Size(81, 20);
            this.toolStripDropDownSetupTable.Text = "Setup Table";
            this.toolStripDropDownSetupTable.Click += new System.EventHandler(this.ToolStripDropDownSetupTable_Click);
            // 
            // ctxMenuSetupTable
            // 
            this.ctxMenuSetupTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItem,
            this.lastBanTimeToolStripMenuItem,
            this.abuseIPDBReportsToolStripMenuItem,
            this.abuseIPDBChecksToolStripMenuItem,
            this.nextTSCacheTimeToolStripMenuItem});
            this.ctxMenuSetupTable.Name = "ctxMenuSetupTable";
            this.ctxMenuSetupTable.OwnerItem = this.toolStripDropDownSetupTable;
            this.ctxMenuSetupTable.ShowImageMargin = false;
            this.ctxMenuSetupTable.ShowItemToolTips = false;
            this.ctxMenuSetupTable.Size = new System.Drawing.Size(151, 114);
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.versionToolStripMenuItem.Text = "Version:";
            this.versionToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lastBanTimeToolStripMenuItem
            // 
            this.lastBanTimeToolStripMenuItem.Name = "lastBanTimeToolStripMenuItem";
            this.lastBanTimeToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.lastBanTimeToolStripMenuItem.Text = "LastBanTime:";
            this.lastBanTimeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // abuseIPDBReportsToolStripMenuItem
            // 
            this.abuseIPDBReportsToolStripMenuItem.Name = "abuseIPDBReportsToolStripMenuItem";
            this.abuseIPDBReportsToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.abuseIPDBReportsToolStripMenuItem.Text = "AbuseIPDBReports:";
            // 
            // abuseIPDBChecksToolStripMenuItem
            // 
            this.abuseIPDBChecksToolStripMenuItem.Name = "abuseIPDBChecksToolStripMenuItem";
            this.abuseIPDBChecksToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.abuseIPDBChecksToolStripMenuItem.Text = "AbuseIPDBChecks:";
            // 
            // nextTSCacheTimeToolStripMenuItem
            // 
            this.nextTSCacheTimeToolStripMenuItem.Name = "nextTSCacheTimeToolStripMenuItem";
            this.nextTSCacheTimeToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nextTSCacheTimeToolStripMenuItem.Text = "NextTSCacheTime:";
            this.nextTSCacheTimeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(57, 17);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(82, 17);
            this.toolStripStatusLabel1.Text = "Shown Values:";
            // 
            // lblShownValues
            // 
            this.lblShownValues.Name = "lblShownValues";
            this.lblShownValues.Size = new System.Drawing.Size(19, 17);
            this.lblShownValues.Text = "$$";
            // 
            // fsWatcher
            // 
            this.fsWatcher.EnableRaisingEvents = true;
            this.fsWatcher.Filter = "prevIpList.sqlite";
            this.fsWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.fsWatcher.SynchronizingObject = this;
            this.fsWatcher.Changed += new System.IO.FileSystemEventHandler(this.FsWatcher_Changed);
            // 
            // timer
            // 
            this.timer.Interval = 3000;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 636);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1050, 323);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AutoBotBlockerViewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ctxMenuListView.ResumeLayout(false);
            this.filterMenuStrip.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ctxMenuSetupTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fsWatcher)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView listView;
        private System.IO.FileSystemWatcher fsWatcher;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtStatement;
        private System.Windows.Forms.LinkLabel llStatement;
        private System.Windows.Forms.Button btnAddFilter;
        private System.Windows.Forms.ColumnHeader cHID;
        private System.Windows.Forms.ColumnHeader chIP;
        private System.Windows.Forms.ColumnHeader chAttempts;
        private System.Windows.Forms.ColumnHeader chTotalAttempts;
        private System.Windows.Forms.ColumnHeader chUnban;
        private System.Windows.Forms.ColumnHeader chLastAttempt;
        private System.Windows.Forms.ColumnHeader chCountry;
        private System.Windows.Forms.ColumnHeader chBanConfidance;
        private System.Windows.Forms.ColumnHeader chBanned;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem reloadDBNowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chbxAutoRefresh;
        private System.Windows.Forms.ToolStripMenuItem chbxHighlight;
        private System.Windows.Forms.ToolStripMenuItem chbxScrollBottom;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblShownValues;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownSetupTable;
        private System.Windows.Forms.ColumnHeader chBanCount;
        private System.Windows.Forms.ContextMenuStrip filterMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem filterDeleteToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ctxMenuSetupTable;
        private System.Windows.Forms.ToolStripMenuItem nextTSCacheTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lastBanTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abuseIPDBChecksToolStripMenuItem;
        private System.Windows.Forms.ListView chlistFilters;
        private System.Windows.Forms.ColumnHeader chExistingFilters;
        private System.Windows.Forms.ContextMenuStrip ctxMenuListView;
        private System.Windows.Forms.ToolStripMenuItem copyDisplayedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyUnderlyingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abuseIPDBReportsToolStripMenuItem;
    }
}

