﻿using AutoBotBlocker.Tables;
using System.Windows.Forms;
using Trienow.Extensions;

namespace AutoBotBlockerViewer
{
    class LVIDBEntry : ListViewItem
    {
        public Main Entry { get; set; }
        public LVIDBEntry(Main entry) : base(new string[]
                    {
                        entry.ID.ToString(),
                        entry.IP,
                        entry.Attempts.ToString(),
                        entry.TotalAttempts.ToString(),
                        entry.Unban.ToDtString(),
                        entry.LastAttempt.ToDtString(),
                        entry.Country,
                        entry.BanConfidance.ToString(),
                        entry.Banned.ToString(),
                        entry.BanCount.ToString()
                    })
        {
            Entry = entry;
        }

        public string UnderlyingFromIndex(int index)
        {
            switch (index)
            {
                case 0:
                    return Entry.ID.ToString();
                case 1:
                    return Entry.IP;
                case 2:
                    return Entry.Attempts.ToString();
                case 3:
                    return Entry.TotalAttempts.ToString();
                case 4:
                    return Entry.Unban.ToString();
                case 5:
                    return Entry.LastAttempt.ToString();
                case 6:
                    return Entry.Country;
                case 7:
                   return Entry.BanConfidance.ToString();
                case 8:
                    return Entry.Banned.ToString();
                case 9:
                    return Entry.BanCount.ToString();
                default:
                    return string.Empty;
            }
        }
    }
}
