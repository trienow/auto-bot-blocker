﻿namespace AutoBotBlockerViewer.Exceptions
{
    public class MissingCommandException : AbstractCommandException
    {
        public MissingCommandException(string command) : base(command + "' ist essenziell, ist aber nicht angegeben.") { }
    }
}
