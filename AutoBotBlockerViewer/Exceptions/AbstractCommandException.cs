﻿using System;

namespace AutoBotBlockerViewer.Exceptions
{
    public abstract class AbstractCommandException : Exception
    {
        protected AbstractCommandException(string message) : base("Der Befehl '" + message) { }
    }
}
