﻿namespace AutoBotBlockerViewer.Exceptions
{
    public class DuplicateCommandException : AbstractCommandException
    {
        public DuplicateCommandException(string command) : base(command + "' darf nur ein Mal angehakt werden.") { }
    }
}
