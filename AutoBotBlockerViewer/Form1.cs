﻿using AutoBotBlocker.Tables;
using AutoBotBlockerViewer.Exceptions;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Trienow.Extensions;

namespace AutoBotBlockerViewer
{
    public partial class Form1 : Form
    {
        private readonly string prevIpPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "prevIpList.sqlite");
        private IDbConnection ipDb;
        private Settings settings;
        private bool writingAllowed = false;

        public Form1()
        {
            InitializeComponent();
        }

        #region [Form]
        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(prevIpPath))
            {
                fsWatcher.Path = Path.GetDirectoryName(prevIpPath);
                ipDb = new SQLiteConnection("DataSource=" + prevIpPath);

                listView.Items.Clear();
                chlistFilters.Items.Clear();
                chlistFilters.ListViewItemSorter = new FilterSorter();

                chbxHighlight.Checked = false;
                ReadSettings();
                ReloadDB();
            }
            else
            {
                MessageBox.Show($"File \"{prevIpPath}\" not found!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            lock (this)
            {
                if (File.Exists(Settings.FILE))
                {
                    WriteSettings();
                }

                try
                {
                    ipDb.Close();
                    ipDb.Dispose();
                }
                catch { }
            }
        }
        #endregion

        #region [Reloading]
        private void FsWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            timer.Stop();
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            ReloadDB();
        }

        private void BtnReload_Click(object sender, EventArgs e)
        {
            ReloadDB();
        }

        private void ReloadDB()
        {
            StringBuilder statement = new StringBuilder();
            List<string> usedCommands = new List<string>(6);

            try
            {
                #region [BUILD SELECT]
                foreach (ListViewItem lvi in chlistFilters.CheckedItems)
                {
                    string filterStatement = lvi.Text;
                    string command = filterStatement.Split(new char[] { ' ' }, 2)[0];

                    if (command != "SELECT")
                    {
                        statement.Append(" ");
                    }

                    if (command == "WHERE")
                    {
                        if (usedCommands.Contains("WHERE"))
                        {
                            filterStatement = "AND" + filterStatement.TrimStart('W', 'H', 'E', 'R', 'E');
                        }

                        if (filterStatement.Contains("Unban") || filterStatement.Contains("LastAttempt"))
                        {
                            filterStatement = filterStatement
                                .Replace("HEUTE", DateTime.Now.Date.Ticks.ToString())
                                .Replace("-24h", DateTime.Now.AddHours(-24).Ticks.ToString())
                                .Replace("+24h", DateTime.Now.AddHours(24).Ticks.ToString())
                                .Replace("MORGEN", DateTime.Now.Date.AddHours(24).Ticks.ToString())
                                .Replace("JETZT", DateTime.Now.Ticks.ToString());
                        }
                    }

                    //Log, that the specific command was used
                    if (!usedCommands.Contains(command))
                    {
                        usedCommands.Add(command);
                    }
                    else if (command != "WHERE")
                    {
                        throw new DuplicateCommandException(command);
                    }

                    statement.Append(filterStatement);
                }

                //Check if essential commands are present
                if (statement.Length == 0)
                {
                    statement.Append("SELECT 0 LIMIT 0");
                }
                else if (!usedCommands.Contains("SELECT"))
                {
                    throw new MissingCommandException("SELECT");
                }
                else if (!usedCommands.Contains("FROM"))
                {
                    throw new MissingCommandException("FROM");
                }
                #endregion

                //Save the last selected item
                int lastSelectedIndex = -1;
                if (listView.SelectedIndices.Count > 0)
                {
                    lastSelectedIndex = listView.SelectedIndices[0];
                }

                //Save all current entries to check for changes
                List<Main> lastEntries = null;
                if (chbxHighlight.Checked)
                {
                    lastEntries = new List<Main>(listView.Items.Count);

                    foreach (ListViewItem lvi in listView.Items)
                    {
                        lastEntries.Add(((LVIDBEntry)lvi).Entry);
                    }
                }

                //Populate the list with query
                listView.Items.Clear();
                foreach (Main entry in ipDb.Query<Main>(statement.ToString()))
                {
                    LVIDBEntry lvi = new LVIDBEntry(entry);

                    if (lastEntries != null && !lastEntries.Contains(entry))
                    {
                        lvi.ForeColor = System.Drawing.Color.Blue;
                    }

                    listView.Items.Add(lvi);
                }

                lblShownValues.Text = listView.Items.Count.ToString();

                //Select and scroll to the last or last selected item
                if (listView.Items.Count > 0)
                {
                    if (chbxScrollBottom.Checked)
                    {
                        listView.Items[listView.Items.Count - 1].EnsureVisible();
                    }

                    if (lastSelectedIndex > 0 && listView.Items.Count > lastSelectedIndex)
                    {
                        listView.Items[lastSelectedIndex].Selected = true;

                        if (!chbxScrollBottom.Checked)
                        {
                            listView.Items[lastSelectedIndex].EnsureVisible();
                        }
                    }
                }
            }
            catch (AbstractCommandException ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString() + "\r\n\r\nStatement:\r\n" + statement.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region [Filter]
        private void LlStatement_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            const string text = @"Jeder Filter ist eine mySQL Klausel, welche mit dem Schlüsselworten SELECT, FROM, WHERE, GROUP BY, ORDER BY oder LIMIT beginnen.
Intern werden alle WHERE Filter nach dem ersten mit einem AND verknüpft. Außer WHERE darf kein anderer Befehl zwei Mal ausgewählt werden!

Die Spalten IP und Country werden als Text behandelt.
Die restlichen Spalten können wie Zahlen behandelt werden (> < <> =).
Für die Datumsspalten können folgende Schlüsselbegriffe verwendet werden:
 - -24h: Jetzige Zeit, vor 24h
 - +24h: Jetzige Zeit, in 24h
 - HEUTE: Verganene Nacht um 0 Uhr
 - MORGEN: Morgen um 0 Uhr
 - JETZT: Jetzt halt :D

Beispiel (Jede Zeile ist ein Filter):
SELECT *
FROM Main
WHERE COUNTRY ='DE'
WHERE LastAttempt > -24h
ORDER BY LastAttempt
LIMIT 10";

            MessageBox.Show(text, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void TxtStatement_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnAddFilter;
        }

        private void TxtStatement_Leave(object sender, EventArgs e)
        {
            AcceptButton = default(IButtonControl);
        }

        private void BtnAddFilter_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtStatement.Text))
            {
                ListViewItem lvi = new ListViewItem(txtStatement.Text);
                lvi.Checked = true;

                chlistFilters.Items.Add(lvi);
                txtStatement.Text = string.Empty;
                WriteSettings();
            }
        }

        private void FilterDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (chlistFilters.SelectedItems.Count >= 0)
            {
                chlistFilters.Items.Remove(chlistFilters.SelectedItems[0]);
                WriteSettings();
            }
        }

        private void ChlistFilters_BeforeLabelEdit(object sender, LabelEditEventArgs e)
        {
            filterDeleteToolStripMenuItem.ShortcutKeys = Keys.None;
        }

        private void ChlistFilters_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            filterDeleteToolStripMenuItem.ShortcutKeys = Keys.Delete;

            if (string.IsNullOrWhiteSpace(e.Label))
            {
                e.CancelEdit = true;
            }
            else
            {
                chlistFilters.Sort();
                WriteSettings();
            }
        }
        #endregion

        #region [Setup Table]
        private void ToolStripDropDownSetupTable_Click(object sender, EventArgs e)
        {
            Setup setup = ipDb.QueryFirstOrDefault<Setup>("SELECT * FROM Setup");

            versionToolStripMenuItem.Text = "Version: " + setup.Version;
            lastBanTimeToolStripMenuItem.Text = "LastBanTime: " + setup.LastBanTime.ToLongDtString();
            abuseIPDBChecksToolStripMenuItem.Text = "AbuseIPDBChecks: " + ipDb.QueryFirst<int>("SELECT COUNT(Time) FROM AbuseIPDBChecks").ToString();
            abuseIPDBReportsToolStripMenuItem.Text = "AbuseIPDBReports: " + ipDb.QueryFirst<int>("SELECT COUNT(Time) FROM AbuseIPDBReports").ToString();
            nextTSCacheTimeToolStripMenuItem.Text = "NextTSCacheTime: " + setup.NextTSCacheTime.ToLongDtString();
        }
        #endregion

        #region [Settings]
        private void ChbxAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            fsWatcher.EnableRaisingEvents = chbxAutoRefresh.Checked;
        }

        private void WriteSettings(object sender = null, EventArgs e = null)
        {
            if (writingAllowed)
            {
                chlistFilters.Sort();

                settings.AutoRefresh = chbxAutoRefresh.Checked ? 1 : 0;
                settings.Highlight = chbxHighlight.Checked ? 1 : 0;
                settings.ScrollBottom = chbxScrollBottom.Checked ? 1 : 0;
                int itemCount = chlistFilters.Items.Count;

                if (itemCount > 0 || settings.FilterStates.Length != itemCount)
                {
                    StringBuilder filters = new StringBuilder();
                    StringBuilder states = new StringBuilder();
                    bool isFirst = true;

                    for (int i = 0; i < chlistFilters.Items.Count; i++)
                    {
                        if (!isFirst)
                        {
                            filters.Append("|");
                        }
                        isFirst = false;

                        ListViewItem lvi = chlistFilters.Items[i];
                        filters.Append(lvi.Text);
                        states.Append(lvi.Checked ? '1' : '0');
                    }
                    settings.Filters = filters.ToString();
                    settings.FilterStates = states.ToString();
                }

                using (IDbConnection settingsDb = GetSettingsConnection())
                {
                    if (settingsDb.QueryFirstOrDefault<int>("SELECT COUNT(ID) FROM Settings WHERE ID = @ID", settings) == 0)
                    {
                        settingsDb.Execute(Settings.INSERT, settings);
                    }
                    else
                    {
                        settingsDb.Execute(Settings.UPDATE, settings);
                    }
                }
            }
        }

        private void ReadSettings()
        {
            bool updateSettings = false;
            writingAllowed = false;

            if (File.Exists(Settings.FILE))
            {
                #region [READ SETTINGS]
                using (IDbConnection settingsDb = GetSettingsConnection())
                {
                    settings = settingsDb.QueryFirstOrDefault<Settings>("SELECT * FROM Settings WHERE ID = @id", new { id = Environment.UserName }) ?? new Settings();

                    if (settings.Version < Settings.CURRENT_VERSION)
                    {
                        if (settings.Version < 2)
                        {
                            settingsDb.Execute("CREATE TABLE IF NOT EXISTS `Settings_v2` (`ID` TEXT NOT NULL PRIMARY KEY UNIQUE, `Version` INTEGER NOT NULL, `AutoRefresh` INTEGER NOT NULL, `Highlight` INTEGER NOT NULL, `ScrollBottom` INTEGER NOT NULL, `Filters` TEXT NOT NULL, `FilterStates` TEXT NOT NULL)");
                            settingsDb.Execute("INSERT INTO Settings_v2 SELECT `ID`,`Version`,`AutoRefresh`,`Highlight`,`ScrollBottom`,`Filters`,`FilterStates` FROM `Settings`");
                            settingsDb.Execute("DROP TABLE `Settings`");
                            settingsDb.Execute("ALTER TABLE `Settings_v2` RENAME TO `Settings`");

                            settings.ID = Environment.UserName;
                            settingsDb.Execute("UPDATE Settings SET ID = @ID WHERE ID = 1", settings);
                        }

                        settings.Version = Settings.CURRENT_VERSION;
                        updateSettings = true;
                    }

                    if (string.IsNullOrWhiteSpace(settings.ID))
                    {
                        settings.ID = Environment.UserName;
                        updateSettings = true;
                    }
                }
                #endregion
            }
            else
            {
                settings = new Settings();
                settings.ID = Environment.UserName;
                updateSettings = true;
            }

            #region [LOAD INTO UI]
            chbxAutoRefresh.Checked = settings.AutoRefresh == 1;
            chbxHighlight.Checked = settings.Highlight == 1;
            chbxScrollBottom.Checked = settings.ScrollBottom == 1;

            string[] filters = settings.Filters.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (filters.Length == settings.FilterStates.Length)
            {
                for (int i = 0; i < settings.FilterStates.Length; i++)
                {
                    ListViewItem lvi = new ListViewItem(filters[i]);
                    lvi.Checked = settings.FilterStates[i] == '1';
                    chlistFilters.Items.Add(lvi);
                }
                chlistFilters.Sort();
            }
            #endregion

            //Save the Settings if they have been modified.
            writingAllowed = true;
            if (updateSettings)
            {
                WriteSettings();
            }
        }

        private IDbConnection GetSettingsConnection()
        {
            IDbConnection settingsDb = new SQLiteConnection("DataSource=" + Settings.FILE);

            settingsDb.Execute(Settings.CREATE);
            settingsDb.Execute("PRAGMA auto_vacuum = \"1\";");

            return settingsDb;
        }
        #endregion

        private void CopyDisplayedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(listView.HitTest(listView.PointToClient(ctxMenuListView.Bounds.Location)).SubItem.Text);
        }

        private void CopyUnderlyingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListViewHitTestInfo hitTest = listView.HitTest(listView.PointToClient(ctxMenuListView.Bounds.Location));
            Clipboard.SetText(((LVIDBEntry)hitTest.Item).UnderlyingFromIndex(hitTest.Item.SubItems.IndexOf(hitTest.SubItem)));
        }
    }
}
