﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Trienow.Extensions;

namespace AutoBotBlockerViewer
{
    class FilterSorter : IComparer
    {
        const string HUMAN_COMPARE = "(?'str'[^0-9]+)|(?'num'[0-9]+)";

        public int Column { get; set; } = 0;
        public int Direction { get; set; } = 1;

        public int Compare(object x, object y)
        {
            ListViewItem lvi1 = x as ListViewItem;
            ListViewItem lvi2 = y as ListViewItem;

            string t1 = lvi1.SubItems[Column].Text;
            string t2 = lvi2.SubItems[Column].Text;
            int result = 0;
            if (t1 != t2)
            {
                //SELECTS go to the top
                result = t2.StartsWith("SELECT ").ToInt() - t1.StartsWith("SELECT ").ToInt();

                //FROMs next
                if (result == 0) result = t2.StartsWith("FROM ").ToInt() - t1.StartsWith("FROM ").ToInt();

                //LIMITs to the bottom
                if(result == 0) result = t1.StartsWith("LIMIT ").ToInt() - t2.StartsWith("LIMIT ").ToInt();

                //ORDER BYs above those
                if (result == 0) result = t1.StartsWith("ORDER BY ").ToInt() - t2.StartsWith("ORDER BY ").ToInt();

                //GROUP BYs above them
                if (result == 0) result = t1.StartsWith("GROUP BY ").ToInt() - t2.StartsWith("GROUP BY ").ToInt();

                if (result == 0)
                {
                    MatchCollection m1 = Regex.Matches(t1, HUMAN_COMPARE, RegexOptions.ExplicitCapture);
                    MatchCollection m2 = Regex.Matches(t2, HUMAN_COMPARE, RegexOptions.ExplicitCapture);
                    int shortest = Math.Min(m1.Count, m2.Count);

                    for (int i = 0; i < shortest; i++)
                    {
                        string text1 = m1[i].Groups[1].Value;
                        string text2 = m2[i].Groups[1].Value;
                        bool numberIn1 = text1 == string.Empty;
                        bool numberIn2 = text2 == string.Empty;

                        if (numberIn1)
                        {
                            if (numberIn2)
                            {
                                //Number comparison if both parts are numbers
                                int i1 = int.Parse(m1[i].Groups[2].Value);
                                int i2 = int.Parse(m2[i].Groups[2].Value);

                                if (i1 < i2)
                                {
                                    result = -1;
                                }
                                else if (i1 > i2)
                                {
                                    result = 1;
                                }
                            }
                            else
                            {
                                //Part 1 is a number, but part 2 is text
                                result = -1;
                            }
                        }
                        else if (numberIn2)
                        {
                            //Part 1 is text, but part 2 is a number
                            result = 1;
                        }
                        else
                        {
                            //String comparison if both parts are strings
                            result = string.Compare(text1, text2, StringComparison.InvariantCulture);
                        }

                        if (result != 0)
                        {
                            break;
                        }
                    }
                }
            }
            return Direction * result;
        }
    }
}
