﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace FirewallUnban
{
    public class FUCore : IDisposable
    {
        /// <summary>
        /// The Database connection. Initialized on construction
        /// </summary>
        public IDbConnection Ipdb { get; private set; }

        /// <summary>
        /// Setup information
        /// </summary>
        public Tables.Setup Setup { get; private set; }

        /// <summary>
        /// The list of IPs to whitelist. Filled via <see cref="LoadIps"/>
        /// </summary>
        public List<string> IpsToUnban { get; private set; }

        private readonly EventLog log;

        /// <summary>
        /// New FUCore containing the main logic
        /// </summary>
        public FUCore(EventLog log)
        {
            this.log = log;

            Ipdb = new SQLiteConnection("DataSource=" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "firewallUnban.sqlite"));

            #region [TABLE CREATION]
            Ipdb.Execute(Tables.IPList.CREATE);
            Ipdb.Execute(Tables.Setup.CREATE);
            #endregion

            #region [GET SETUP VALUES]
            Setup = Ipdb.QueryFirstOrDefault<Tables.Setup>(Tables.Setup.SELECT);
            if (Setup == null)
            {
                Setup = new Tables.Setup();
                Ipdb.Execute(Tables.Setup.INSERT, Setup);
            }
            #endregion

            // N/A for now
            #region [VERSION UPGRADE]
            #endregion
        }

        /// <summary>
        /// Removes IPs, which have surpassed their TTL
        /// </summary>
        /// <returns>The amount of IPs not active anymore</returns>
        public int WeedOutDb()
        {
            //Weed out table
           return Ipdb.Execute(Tables.IPList.DELETE, new { ticks = DateTime.Now.Ticks });
        }

        /// <summary>
        /// Reads the remote list of IPs to unban and writes these directly into the DB
        /// </summary>
        /// <returns>The amount of IPs found to unban</returns>
        public int RemoteListIntoDb(string unbanListUrl)
        {
            int newIps = 0;

            if (!string.IsNullOrWhiteSpace(unbanListUrl))
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string NewIpsToUnbanString = new WebClient().DownloadString(unbanListUrl).Trim();
                string[] newIpsToUnban = NewIpsToUnbanString.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                newIps = newIpsToUnban.Length;
                if (newIps > 0)
                {
                    log?.WriteEntry("New IPs to unban: " + NewIpsToUnbanString, EventLogEntryType.Information, 100);
                }

                foreach (string newIp in newIpsToUnban)
                {
                    if (IPAddress.TryParse(newIp, out _))
                    {
                        Ipdb.Execute(Tables.IPList.REPLACE, new Tables.IPList() { IP = newIp });
                    }
                    else
                    {
                        log?.WriteEntry("Invalid IP: " + newIp, EventLogEntryType.Warning, 500);
                    }
                }
            }

            return newIps;
        }

        /// <summary>
        /// Loads all stored IPs from the Database into Memory
        /// </summary>
        public void LoadIps()
        {
            IpsToUnban = new List<string>(Ipdb.Query<string>(Tables.IPList.SELECT)) ?? new List<string>();
        }

        public void WriteFirewallRules(string additionalIps)
        {
            FirewallRule rule = new FirewallRule()
            {
                Description = "The RDP whitelist",
                Enabled = true,

                RemoteAddressesList = IpsToUnban,

                LocalPorts = "3389",
                ServiceName = "TermService",
                ApplicationName = @"%SystemRoot%\system32\svchost.exe"
            };

            if (!string.IsNullOrWhiteSpace(additionalIps))
            {
                rule.RemoteAddresses += "," + additionalIps;
            }

            try
            {

                rule.Name = "03389 TCP RDP";
                rule.Protocol_Enum = NetFwTypeLib.NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
                rule.Apply();

                rule.Name = "03389 UDP RDP";
                rule.Protocol_Enum = NetFwTypeLib.NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_UDP;
                rule.Apply();
            }
            catch (Exception ex)
            {
                log?.WriteEntry("Error in \r\n" + rule.ToString() + "\r\n\r\nException:\r\n" + ex.ToString(), EventLogEntryType.Error, 401);
                throw ex;
            }

            FirewallRule shadowRule = new FirewallRule()
            {
                Name = "03389 TCP RDP Shadow",
                Description = "The RDP whitelist",
                Enabled = true,

                Protocol_Enum = NetFwTypeLib.NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP,

                RemoteAddresses = rule.RemoteAddresses,
                ApplicationName = @"%SystemRoot%\system32\RdpSa.exe",

                ServiceName = "TermService"
            };

            try
            {
                shadowRule.Apply();
            }
            catch (Exception ex)
            {
                log?.WriteEntry("Error in \r\n" + shadowRule.ToString() + "\r\n\r\nException:\r\n" + ex.ToString(), EventLogEntryType.Error, 402);
            }
        }

        public void Dispose()
        {
            lock (this)
            {
                try
                {
                    Ipdb.Close();
                    Ipdb.Dispose();
                }
                catch { }
            }
        }

        ~FUCore() => Dispose();
    }
}
