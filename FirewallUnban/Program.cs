﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace FirewallUnban
{
    class Program
    {
        static void Main(string[] args)
        {
            string unbanListUrl = ConfigurationManager.AppSettings["UnbanListURL"];
            string permanentlyWhitelisted = ConfigurationManager.AppSettings["PermanentlyWhitelisted"];

            EventLog log = new EventLog();
            #region [LOGGER]
            if (!EventLog.SourceExists("FirewallUnban"))
            {
                EventLog.CreateEventSource("FirewallUnban", "FirewallUnban Log");
            }

            log.Source = "FirewallUnban";
            log.Log = "FirewallUnban Log";
            #endregion

            try
            {
                FUCore core = new FUCore(log);
                bool changesFound = core.WeedOutDb() > 0;
                changesFound |= core.RemoteListIntoDb(unbanListUrl) > 0;

                if (changesFound)
                {
                    core.LoadIps();
                    core.WriteFirewallRules(permanentlyWhitelisted);
                }
            }
            catch (Exception ex)
            {
                log.WriteEntry(ex.ToString(), EventLogEntryType.Error, 400);
            }
        }
    }
}
