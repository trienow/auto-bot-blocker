﻿using NetFwTypeLib;
using System;
using System.Collections.Generic;

namespace FirewallUnban
{
    public class FirewallRule
    {
        private static INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

        public string Name { get; set; }
        public string Description { get; set; }
        public string Grouping { get; set; }
        public bool Enabled { get; set; } = false;

        public NET_FW_IP_PROTOCOL_ Protocol_Enum { set => Protocol = (int)value; }
        public int Protocol { get; set; } = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_ANY;

        public string RemoteAddresses { get; set; }
        public IEnumerable<string> RemoteAddressesList { set => RemoteAddresses = string.Join(",", value); }

        public string LocalAddresses { get; set; }
        public IEnumerable<string> LocalAddressesList { set => LocalAddresses = string.Join(",", value); }

        public NET_FW_ACTION_ Action { get; set; } = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
        public NET_FW_RULE_DIRECTION_ Direction { get; set; } = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;

        public int Profiles { get; set; } = (int)NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_ALL;
        public NET_FW_PROFILE_TYPE2_ ProfilesEnum { set => Profiles = (int)value; }

        public string RemotePorts { get; set; }
        public IEnumerable<string> RemotePortsList { set => RemotePorts = string.Join(",", value); }

        public string LocalPorts { get; set; }
        public IEnumerable<string> LocalPortsList { set => LocalPorts = string.Join(",", value); }

        public string ServiceName { get; set; }
        public string ApplicationName { get; set; }

        public FirewallRule() { }

        public FirewallRule(INetFwRule fwRule)
        {
            if (fwRule != null)
            {
                Name = fwRule.Name;
                Description = fwRule.Description;
                Grouping = fwRule.Grouping;
                Enabled = fwRule.Enabled;

                Protocol = fwRule.Protocol;
                RemoteAddresses = fwRule.RemoteAddresses;

                Action = fwRule.Action;
                Direction = fwRule.Direction;
                Profiles = fwRule.Profiles;

                RemotePorts = fwRule.RemotePorts;
                LocalPorts = fwRule.LocalPorts;

                ServiceName = fwRule.serviceName;
                ApplicationName = fwRule.ApplicationName;
            }
        }

        public FirewallRule(string name) : this(GetExistingRule(name))
        {
            Name = name;
        }

        public INetFwRule Apply()
        {
            INetFwRule fwRule = GetExistingRule(Name);

            bool isNewRule = fwRule == null;
            if (isNewRule)
            {
                //If rule does not exist yet: Create new
                fwRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            }

            //Write Properties
            fwRule.Name = Name;
            fwRule.Description = Description;
            fwRule.Grouping = Grouping;
            fwRule.Enabled = Enabled;

            fwRule.Protocol = Protocol;
            fwRule.RemoteAddresses = RemoteAddresses;
            fwRule.LocalAddresses = LocalAddresses;

            fwRule.Action = Action;
            fwRule.Direction = Direction;
            fwRule.Profiles = Profiles;

            fwRule.RemotePorts = RemotePorts;
            fwRule.LocalPorts = LocalPorts;

            fwRule.serviceName = ServiceName;
            fwRule.ApplicationName = ApplicationName;

            //If the rule is new, add it!
            if (isNewRule)
            {
                firewallPolicy.Rules.Add(fwRule);
            }

            return fwRule;
        }

        public override string ToString()
        {
            return "{" +
                $"{nameof(Name)}: '{Name}'," +
                $"{nameof(Description)}: '{Description}', " +
                $"{nameof(Grouping)}: '{Grouping}', " +
                $"{nameof(Enabled)}: '{Enabled}', " +
                $"{nameof(Protocol)}: '{Protocol}', " +
                $"{nameof(RemoteAddresses)}: '{RemoteAddresses}', " +
                $"{nameof(LocalAddresses)}: '{LocalAddresses}', " +
                $"{nameof(Action)}: '{Action}', " +
                $"{nameof(Direction)}: '{Direction}', " +
                $"{nameof(Profiles)}: '{Profiles}', " +
                $"{nameof(RemotePorts)}: '{RemotePorts}', " +
                $"{nameof(LocalPorts)}: '{LocalPorts}', " +
                $"{nameof(ServiceName)}: '{ServiceName}', " +
                $"{nameof(ApplicationName)}: '{ApplicationName}'" +
                "}";
        }

        public static INetFwRule GetExistingRule(string name)
        {
            INetFwRule fwRule = null;
            try
            {
                //Check if rule already exists
                fwRule = firewallPolicy.Rules.Item(name);
            }
            catch (System.IO.FileNotFoundException) { }

            return fwRule;
        }
    }
}
