﻿namespace FirewallUnban.Tables
{
    public class Setup
    {
        public const int DB_VERSION = 0;

        public const string CREATE = "CREATE TABLE IF NOT EXISTS `Setup` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `Version` INTEGER NOT NULL);";
        public const string SELECT = "SELECT * FROM `Setup`;";
        public const string INSERT = "INSERT INTO `Setup` VALUES(@ID, @Version);";
        public const string UPDATE = "UPDATE `Setup` SET `Version` = @Version WHERE ID = @ID;";

        public int ID { get; set; } = 1;
        public int Version { get; set; } = DB_VERSION;
    }
}
