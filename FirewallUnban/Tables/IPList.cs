﻿using System;

namespace FirewallUnban.Tables
{
    public class IPList
    {
        public const int ACTIVE_DAYS = 7;

        public const string CREATE = "CREATE TABLE IF NOT EXISTS `IPList` (`IP` TEXT NOT NULL, `ActiveUntil` INTEGER NOT NULL, PRIMARY KEY(`IP`));";

        public const string SELECT = "SELECT IP FROM `IPList`;";
        public const string REPLACE = "REPLACE INTO `IPList` VALUES(@IP, @ActiveUntil);";
        public const string DELETE = "DELETE FROM `IPList` WHERE ActiveUntil < @ticks;";

        public string IP { get; set; }
        public long ActiveUntil { get; set; } = DateTime.Now.AddDays(ACTIVE_DAYS).Ticks;
    }
}
